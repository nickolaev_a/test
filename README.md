## Установка и запуск

1. git clone git@bitbucket.org:nickolaev_a/test.git
2. cd test
3. sudo -i
4. ./install.sh

Последующие запуски через
docker-compose up -d


Посмотреть базу можно через adminer: http://test.local:8080/  
login: postgres
password: postgres
database: postgres


site: http://test.local:80/


    login: admin@admin.ru
    pass: admin

	login: user@user.ru
	pass: user


Файлы для тестирования:

    test_1.xml	- валидный с одним челом
    test_2.xml - валидный с двумя челами
    test_wrong.xml - совсем не валидный
