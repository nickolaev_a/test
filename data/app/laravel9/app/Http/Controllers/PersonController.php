<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Library\DOMValidator;
use App\Models\Person;


class PersonController extends Controller
{
    //
    /**
     * Display the view with upload form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('upload', ['request' => $request]);
    }



    /**
     * Display the registry.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function registry(Request $request)
    {
        $user_id =  Auth::user()->getAuthIdentifier();
        $person_obj = new Person();
        $persons = $person_obj->where('user_id', $user_id)->get();

        return view('registry', ['persons' => $persons]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $xsd_file = 'persons.xsd';
        $cnt_persons = 0;

        if ($request->file('file')) {
            $xml_file = $request->file->path();
            $shema_file = storage_path('app/schemas').'/'.$xsd_file;

            if(is_file($shema_file)) {
                $validator = new DOMValidator($shema_file);
                $validated = $validator->validateFeeds($xml_file);

                if ($validated) {
                    // Feed successfully validated
                    $user_id =  Auth::user()->getAuthIdentifier();

                    $xml_string = file_get_contents($xml_file);
                    $xml = new \SimpleXMLElement($xml_string, 0, false, "tns", true);
                    if (count($xml) < 1) {
                        return back()->withErrors(['no data']);
                    } else {
                        // save in database
                        foreach ($xml as $item) {
                            $person = new Person();
                            $person->lastname = $item->lastname;
                            $person->firstname = $item->firstname;
                            $person->middleName = $item->middleName;
                            $person->user_id = $user_id;
                            $person->save();
                            $cnt_persons++;
                        }
                    }

                } else {
                    return back()->withErrors($validator->displayErrors());
                }

            } else {
                return back()->withErrors(['can\'t validate. Schema not found.']);
            }

        }

        return back()->with('message', $cnt_persons.' persons uploaded successfully');

    }


}
