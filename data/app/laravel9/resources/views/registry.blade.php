<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Registry') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="p-6 bg-white border-b border-gray-200">
                <table class="w-full border-separate border border-slate-400">
                    <thead>
                    <tr>
                        <th class="border border-slate-300"> №</th>
                        <th class="border border-slate-300"> Фамилия</th>
                        <th class="border border-slate-300"> Имя</th>
                        <th class="border border-slate-300"> Отчество</th>
                        <th class="border border-slate-300"> Дата загрузки</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($persons as $person)
                        <tr>
                            <td class="border border-slate-300 text-center"> {{$person->id}} </td>
                            <td class="border border-slate-300 sm:px-6 lg:px-8"> {{$person->lastname}} </td>
                            <td class="border border-slate-300 sm:px-6 lg:px-8"> {{$person->firstname}} </td>
                            <td class="border border-slate-300 sm:px-6 lg:px-8"> {{$person->middleName}} </td>
                            <td class="border border-slate-300 text-center"> {{ date("j.m.Y", strtotime($person->created_at))}} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
