<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Upload') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('person.file.upload') }}" aria-label="{{ __('Upload') }}"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="p-4">
                            <div class="flex justify-center">
                                <div class="mb-3 w-96">
                                    <label for="formFile"
                                           class="form-label block mb-2 text-gray-700">{{ __('Select xml file') }}</label>
                                    <input class="form-control inline-block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0
 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" type="file" name="file"
                                           id="formFile" accept=".xml">
                                    <button type="submit" class="inline-block bg-gray-100 border border-solid border-gray-300
 rounded border border-solid border-gray-300 text-lg  bg-blue-600">{{ __('Upload') }}</button>

                                    @if ($errors->any())
                                        <div class="bg-red-300 aspect-square">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(session()->get('message'))
                                        <div class="bg-green-300 aspect-square">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    <div class="mb-3 w-96">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-4">
                            <div class="flex space-x-2 justify-center">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
