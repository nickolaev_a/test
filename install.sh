#!/bin/bash

docker-compose stop -t 1

# buils
docker-compose up --build -d

# wait to load services
sleep 10s

# init db
docker exec l9test.local php artisan migrate
docker exec l9test.local php artisan db:seed
